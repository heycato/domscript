dom lib inspired from [here](https://hackernoon.com/how-i-converted-my-react-app-to-vanillajs-and-whether-or-not-it-was-a-terrible-idea-4b14b1b2faff#.lzuz56umj)

### domscript usage

```javascript
const dom = require('domscript')

const { div, span, p } = dom.dom

document.body.appendChild(
  div({ id:'container' },
    div({ className:'first' },
      span('I\'m first')
    ),
    div({ className:'second' },
      p('I\'m second')
    )
  )
)
```

gives us this in the dom:

```html
<div id='container'>
  <div class='first'>
    <span>I'm first</span>
  </div>
  <div class='second'>
    <p>I'm second</p>
  </div>
</div>
```

### messenger usage

```javascript
// create a "Messenger" object
const msgr = dom.createMessenger()

// subscribe to an event
msgr.on('some-custom-event', (data) => {
  console.log('some-custom-event triggered:', data)
})

// emit an event
msgr.emit('some-custom-event', { some: { cool: { data: 'yo' } } })

// unsubscribe
msgr.off('some-custom-event', (data) => {
  console.log('some-custom-event triggered:', data)
})
```

### store usage

```javascript
const store = dom.createStore()

store.set('some value', 'some', 'nested', 'key')

store.get('some', 'nested') //<- { key:'some value' }

store.getState() //<- { some: { nested: { key: 'some value' } } }

store.del('some', 'nested')

store.getState() //<- { some: {} }
```

### component usage

```javascript
// component requires an instance of Messenger
const msgr = dom.createMessenger()
const component = dom.createComponent.bind(null, msgr)

// like a todo list for example
const createTodoItem = (data) =>
  div({id: data.id, className: 'todo-item'},
    div({className:'todo-description'}, data.desc),
    div({className:'todo-complete'},
      button({id: `${data.id}-complete-button`},
        data.complete
      )
    ),
    div({className:'todo-remove'},
      button({id: `${data.id}-remove-button`}, 'remove')
    )
  )

const todoList = component(
  'todo-list', //<- str name of component
  (data) => div(data.map(createTodoItem)), //<- render function
  [] //<- initial data
)
```
