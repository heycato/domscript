import util from './lib/util.js'
import htmltags from './lib/htmltags.js'
import createMessenger from './lib/messenger.js'
import createStore from './lib/store.js'
import createComponent from './lib/component.js'
import createDomTags from './lib/dom.js'

const dom = createDomTags(this, this.document, htmltags.tags)

export default  { dom:dom
                , createMessenger:createMessenger
                , createComponent:createComponent
                , createStore:createStore }
