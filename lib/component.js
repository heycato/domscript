export default (msgr, name, render, initData, noUpdate) => {

  let el = render(initData)

  const update = (prevEl, newData) => {
    const nextEl = render(newData)
    if(!nextEl.isEqualNode(prevEl)) {
      prevEl.parentElement.replaceChild(nextEl, prevEl)
      return nextEl
    } else {
      msgr.emit('warn', {
        message: `render() called but no changed detected for: ${name}`
      })
      return prevEl
    }
  }
  
  if(!noUpdate) {
    msgr.on('update', (data) => {
      msgr.emit('info', {
        message: `Updating: ${name} with ${JSON.stringify(data)}` 
      })
      el = update(el, data)
    })
  }

  return el
}
