const clone = (obj) => {
  if(obj === undefined) return
  try {
    return JSON.parse(JSON.stringify(obj))
  } catch(e) { 
    console.error(e)
    return
  }
}

export default (model) => ({
  getState() { return clone(model) }, 
  get(path) {
    return clone(path.split('.')
      .reduce((acc, key) => acc ? acc[key] : undefined, model))
  },
  set(value, path) {
    let pathArr = path.split('.')
    let len = pathArr.length
    return clone(pathArr.reduce((acc, key, i) => {
      if(acc && typeof acc[key] !== 'object') acc[key] = {}
      if(acc && i === len - 1) acc[key] = value
      return acc[key]
    }, model))
  },
  del(path) {
    let pathArr = path.split('.')
    let len = pathArr.length
    return pathArr.reduce((acc, key, i) => {
      if(acc && i === len - 1) delete acc[key]
      return acc[key]
    }, model)
  }
})
