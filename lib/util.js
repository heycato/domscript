const getType = (obj) =>
  Object.prototype.toString.apply(obj)
    .replace('[object ', '')
    .replace(']', '')
    .toLowerCase()

const flip = (fn, ...args) => fn.apply(null, args.reverse())

const prop = (key, obj) => 
  obj !== undefined ? obj[key] : undefined

const path = (obj, strOrArr) => {
  if(Array.isArray(strOrArr)) {
    return strOrArr.reduce(flip.bind(null, prop), obj)
  } else if(getType(strOrArr) === 'string') {
    return strOrArr.split('.').reduce(flip.bind(null, prop), obj)
  }
  return undefined
}

export default  { getType
                , flip
                , prop
                , path }
