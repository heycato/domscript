const attributeExceptions = ['role', 'attribute']

const htmlStrToElement = (doc, str) => {
  const template = doc.createElement('template')
  template.innerHTML = str
  return template.content.firstChild
}

const isHtmlStr = (str) => /<[a-z][\s\S]*>|[^&(.*);$]/i.test(str)
const isHtmlSpecial = (str) => /^\&(.*)\;$/i.test(str)

const childAppender = (win, doc, parent, child) => {
  if(Array.isArray(child)) {
    child.reduce(childAppender.bind(null, win, doc), parent)
  } else if(child instanceof win.Element) {
    parent.appendChild(child)
  } else if(typeof child === 'string') {
    if(isHtmlStr(child) || isHtmlSpecial(child)) {
      parent.appendChild(htmlStrToElement(doc, child))
    } else {
      parent.appendChild(doc.createTextNode(child))
    }
  }
  return parent
}

const setStyles = (el, styles) => {
  if(!styles) {
    el.removeAttribute('styles')
    return el
  }
  Object.keys(styles)
    .map((prop) => {
      if(prop in el.style) {
        el.style[prop] = styles[prop]
      } else {
        console.warn(`${styleName} is not a valid style for a <${el.tagName.toLowerCase()}>`);
      } 
    })
  return el
}

const setAttributes = (el, attrs) => {
  Object.keys(attrs)
    .map((prop) => {
      if(attrs[prop] === undefined) {
        el.removeAttribute(prop)
      } else {
        el.setAttribute(prop, attrs[prop])
      }
    })
  return el
}

const createElement = (win, doc, type, textOrPropsOrChild, ...otherChildren) => {
  const el = type === 'fragment' ? doc.createDocumentFragment() : doc.createElement(type)
  const appendChild = childAppender.bind(null, win, doc)

  if(Object.prototype.toString.apply(textOrPropsOrChild) === '[object Object]') {
    Object.keys(textOrPropsOrChild)    
      .map((name) => {
        if(name in el || attributeExceptions.includes(name)) {
          const value = textOrPropsOrChild[name]
          if(name === `style`) {
            setStyles(el, value)
          } else if(name === `attribute`) {
            setAttributes(el, value)
          } else if(value) {
            el[name] = value
          }
        } else {
          console.warn(`${name} is not a valid property of a <${type}>`)
        }
      })
  } else {
    appendChild(el, textOrPropsOrChild)
  }
  
  if(otherChildren) appendChild(el, otherChildren)
  return el
}

export default (window, document, tagStrs) => 
  tagStrs
    .reduce((acc, tag) => {
      acc[tag] = createElement.bind(null, window, document, tag)
      return acc
    }, {})
