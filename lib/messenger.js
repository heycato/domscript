const GLOBAL = this

const getDebugMsgs = () =>
  ['error', 'warn', 'info']
    .map((type) => ({
      type: new RegExp(type, 'gi'),
      cb: (msg) => {
        if(GLOBAL.APP_DEBUG) console[type](`${type.toUpperCase()}:`, msg)
      }
    }))

class Messenger {
  constructor() { this.messages = getDebugMsgs() }
  on(type, cb) { this.messages.push({type, cb}) }
  off(type, cb) {
    this.messages = this.messages
      .filter((msg) =>
        msg.type.toString() !== type.toString() ||
        msg.cb.toString() !== cb.toString()
      )
  }
  emit(type, payload) {
    this.messages
      .forEach((msg) => {
        if(Object.prototype.toString.apply(msg.type) === '[object RegExp]') {
          if(msg.type.test(type)) {
            msg.cb.apply(null, [payload])
          }
        } else if(msg.type.toString() === type.toString()) {
          msg.cb.apply(null, [payload])
        }
      })
  }
}

export default () => new Messenger()
